/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    domains: ['avatars.githubusercontent.com', 'cloudflare-ipfs.com', 'localhost:5010'],
    remotePatterns: [
      {
        protocol: 'http',
        hostname: 'localhost',
        port: '5010',
        pathname: '**'
      }
    ]
  }
}

export default nextConfig
