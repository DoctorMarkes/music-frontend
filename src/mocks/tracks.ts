import { faker } from '@faker-js/faker'
export interface IComment {
  _id: string
  username: string
  text: string
}

export interface ITrack {
  _id: string
  name: string
  artist: string
  text: string
  listens: number
  picture: string
  comments: IComment[]
  audio: string
}

export const generateRandomTrack = (): ITrack => {
  return {
    _id: faker.string.uuid(),
    name: faker.music.songName(),
    artist: faker.person.firstName(),
    text: faker.lorem.paragraph(30),
    listens: faker.number.int({ min: 0, max: 1000 }),
    picture: faker.image.avatar(),
    audio:
      'http://localhost:5010/audio/a50782df-1a73-4320-9fe1-87212c27a8f5.mp3',
    comments: generateRandomComments(),
  }
}

const generateRandomComments = (count: number = 3): IComment[] => {
  const comments: IComment[] = []
  for (let i = 0; i < count; i++) {
    comments.push({
      _id: faker.string.uuid(),
      username: faker.internet.userName(),
      text: faker.lorem.sentence(),
    })
  }
  return comments
}

export const generateRandomTrackArray = (count: number = 3): ITrack[] => {
  const trackArray: ITrack[] = []
  for (let i = 0; i < count; i++) {
    trackArray.push(generateRandomTrack())
  }
  return trackArray
}
