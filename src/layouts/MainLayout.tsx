import React from 'react'
import Navbar from '../components/Navbar'
import Head from 'next/head'
import { Container } from '@mui/material'
import Player from '@/components/Player'

export interface MainLayoutProps {
  title?: string
  description?: string
  keywords?: string
  children: React.ReactNode
}

const MainLayout: React.FC<MainLayoutProps> = ({ children }) => {
  return (
    <>
      <Head>
        <meta name="robots" content="index, follow" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
      </Head>
      <Navbar />
      <Container style={{ margin: '90px auto' }}>{children}</Container>
      <Player />
    </>
  )
}

export default MainLayout
