import React, { FC, useCallback, useEffect } from 'react'
import { Pause, PlayArrow, VolumeUp } from '@mui/icons-material'
import { Grid, IconButton } from '@mui/material'
import styles from '../styles/Player.module.scss'
import TrackProgress from '@/components/TrackProgress'
import { useAppDispatch, useAppSelector } from '@/lib/hooks'
import {
  pauseTrack,
  playTrack,
  setCurrentTime,
  setDuration,
  setVolume,
} from '@/app/GlobalRedux/playerSlice'
import { getFilePath } from '@/lib/utils'

interface IPlayerProps {}

let audio: HTMLAudioElement

const Player: FC<IPlayerProps> = () => {
  const { pause, volume, duration, currentTime, active } = useAppSelector(
    (state) => state.player,
  )
  const dispatch = useAppDispatch()

  const setAudio = useCallback(() => {
    if (active) {
      audio.src = getFilePath(active.audio)
      audio.volume = volume / 100
      audio.onloadedmetadata = () => {
        dispatch(setDuration(Math.ceil(audio.duration)))
      }
      audio.ontimeupdate = () => {
        dispatch(setCurrentTime(Math.ceil(audio.currentTime)))
      }
      dispatch(playTrack())
      audio.play()
    } else {
      audio.pause()
    }
  }, [active, dispatch, volume])

  const handlePlay = () => {
    if (pause) {
      dispatch(playTrack())
      audio.play()
    } else {
      dispatch(pauseTrack())
      audio.pause()
    }
  }

  useEffect(() => {
    if (!audio) {
      audio = new Audio()
    } else {
      setAudio()
    }
  }, [dispatch, setAudio])

  const handleChangeVolume = (e: React.ChangeEvent<HTMLInputElement>) => {
    const value = Number(e.target.value)
    audio.volume = value / 100
    dispatch(setVolume(value))
  }

  const handleChangeCurrentTime = (e: React.ChangeEvent<HTMLInputElement>) => {
    const value = Number(e.target.value)
    audio.currentTime = value
    dispatch(setCurrentTime(value))
  }

  if (!active) return null

  return (
    <div className={styles.player}>
      <IconButton onClick={handlePlay}>
        {pause ? <PlayArrow /> : <Pause />}
      </IconButton>
      <Grid
        style={{ width: 200, margin: '0 20px' }}
        container
        direction={'column'}
      >
        <div>{active?.name}</div>
        <div style={{ fontSize: 12, color: 'gray' }}>{active?.artist}</div>
      </Grid>
      <TrackProgress
        left={currentTime}
        right={duration}
        onChange={handleChangeCurrentTime}
      />
      <VolumeUp style={{ marginLeft: 'auto' }} />
      <TrackProgress left={volume} right={100} onChange={handleChangeVolume} />
    </div>
  )
}

Player.propTypes = {}

export default Player
