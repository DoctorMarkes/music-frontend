import React, { FC, MouseEventHandler } from 'react'
import { ITrack } from '@/types/track'
import styles from '../styles/TrackItem.module.scss'
import { Card, Grid, IconButton } from '@mui/material'
import { Delete, Pause, PlayArrow } from '@mui/icons-material'
import Image from 'next/image'
import { useRouter } from 'next/navigation'
import { useAppDispatch, useAppSelector } from '@/lib/hooks'
import { pauseTrack, playTrack, setActive } from '@/app/GlobalRedux/playerSlice'
import { getFilePath } from '@/lib/utils'
import { useDeleteTrackMutation } from '@/services/track'

interface ITrackItem {
  track: ITrack
  active?: boolean
}

const TrackItem: FC<ITrackItem> = ({ track }) => {
  const router = useRouter()
  const dispatch = useAppDispatch()
  const { active, currentTime, duration } = useAppSelector(
    (state) => state.player,
  )
  const [deleteTrack] = useDeleteTrackMutation()

  const isActive = active?._id === track._id

  const handleClick = () => router.push(`/tracks/${track._id}`)

  const handlePlay = (e: React.ChangeEvent<unknown>) => {
    e.stopPropagation()
    if (isActive) {
      dispatch(setActive(null))
      dispatch(pauseTrack())
    } else {
      dispatch(setActive(track))
      dispatch(playTrack())
    }
  }

  const handleDelete = async (e: React.ChangeEvent<unknown>) => {
    e.stopPropagation()
    await deleteTrack(track._id)
  }

  return (
    <Card className={styles.track} onClick={handleClick}>
      <IconButton onClick={handlePlay} style={{ marginRight: '12px' }}>
        {track._id === active?._id ? <Pause /> : <PlayArrow />}
      </IconButton>
      <Image
        width={70}
        height={70}
        style={{ flexShrink: 0 }}
        src={getFilePath(track.picture)}
        alt={'picture'}
      />
      <Grid className={styles.description} container direction={'column'}>
        <div>{track.name}</div>
        <div className={styles.artist}>{track.artist}</div>
      </Grid>
      {isActive && (
        <div style={{ minWidth: 100 }}>
          {currentTime} / {duration}
        </div>
      )}
      <IconButton className={styles.delete} onClick={handleDelete}>
        <Delete />
      </IconButton>
    </Card>
  )
}

export default TrackItem
