'use client'
import * as React from 'react'
import MenuIcon from '@mui/icons-material/Menu'
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft'
import InboxIcon from '@mui/icons-material/MoveToInbox'
import {
  AppBar,
  Drawer,
  List,
  IconButton,
  ListItemText,
  ListItemIcon,
  Toolbar,
  Typography,
  ListItemButton,
} from '@mui/material'
import MailIcon from '@mui/icons-material/Mail'
import { useRouter } from 'next/navigation'

const menuItems = [
  { text: 'Главная', href: '/' },
  { text: 'Список треков', href: '/tracks' },
  { text: 'Список альбомов', href: '/albums' },
] as const

export default function Navbar() {
  const [open, setOpen] = React.useState(false)
  const router = useRouter()

  const handleDrawerOpen = () => {
    setOpen(true)
  }

  const handleDrawerClose = () => {
    setOpen(false)
  }

  return (
    <div>
      <AppBar position={'fixed'}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap component="div">
            Persistent drawer
          </Typography>
        </Toolbar>
      </AppBar>
      <Drawer variant="persistent" anchor="left" open={open}>
        <div>
          <IconButton onClick={handleDrawerClose}>
            <ChevronLeftIcon />
          </IconButton>
        </div>
        <List>
          {menuItems.map(({ text, href }, index) => (
            <ListItemButton key={href} onClick={() => router.push(href)}>
              <ListItemIcon>
                {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
              </ListItemIcon>
              <ListItemText primary={text} />
            </ListItemButton>
          ))}
        </List>
      </Drawer>
    </div>
  )
}
