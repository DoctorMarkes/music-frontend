import React, { FC } from 'react'

interface ITrackProgress {
  left: number
  right: number
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void
}

const TrackProgress: FC<ITrackProgress> = ({ left, right, onChange }) => {
  return (
    <div
      style={{
        display: 'flex',
        minWidth: 200,
        justifyContent: 'space-between',
      }}
    >
      <input
        min={0}
        max={right}
        value={left}
        type={'range'}
        onChange={onChange}
      />
      <div>
        {left} / {right}
      </div>
    </div>
  )
}

export default TrackProgress
