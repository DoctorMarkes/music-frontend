import { useDispatch, useSelector, useStore } from 'react-redux'
import type { TypedUseSelectorHook } from 'react-redux'
import { AppDispatch, AppStore, RootState } from '@/app/GlobalRedux/store'
import React, { useState } from 'react'

export const useAppDispatch: () => AppDispatch = useDispatch
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector
export const useAppStore: () => AppStore = useStore

export const useInput = <T>(initialValue: T) => {
  const [value, setValue] = useState<T>(initialValue)

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setValue(e.target.value as T)
  }

  return { value, onChange }
}
