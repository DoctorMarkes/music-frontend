'use client'
import React, { useState } from 'react'
import MainLayout from '@/layouts/MainLayout'
import StepWrapper from '@/components/StepWrapper'
import { Button, Grid, TextField } from '@mui/material'
import FileUpload from '@/components/FileUpload'
import { useInput } from '@/lib/hooks'
import { useCreateTrack } from '@/services/track'
import { useRouter } from 'next/navigation'
import { ITrack } from '@/types/track'

export default function Page() {
  const [activeStep, setActiveStep] = useState(0)
  const [picture, setPicture] = useState(null)
  const [audio, setAudio] = useState(null)
  const name = useInput('')
  const artist = useInput('')
  const text = useInput('')
  const [createTrack] = useCreateTrack()
  const router = useRouter()

  const handleNext = async () => {
    if (activeStep !== 2) {
      setActiveStep((prev) => prev + 1)
    } else {
      const formData = new FormData()
      formData.append('name', name.value)
      formData.append('text', text.value)
      formData.append('artist', artist.value)
      formData.append('picture', picture!)
      formData.append('audio', audio!)
      await createTrack(formData as Partial<ITrack>)
      router.push('/tracks')
    }
  }

  const handleBack = () => {
    setActiveStep((prev) => prev - 1)
  }

  return (
    <MainLayout>
      <StepWrapper activeStep={activeStep}>
        {activeStep === 0 && (
          <Grid container direction={'column'} style={{ padding: 20 }}>
            <TextField
              {...name}
              label={'Название трека'}
              style={{ marginTop: 10 }}
            />
            <TextField
              {...artist}
              label={'Имя исполнителя'}
              style={{ marginTop: 10 }}
            />
            <TextField
              {...text}
              label={'Слова к треку'}
              multiline
              rows={3}
              style={{ marginTop: 10 }}
            />
          </Grid>
        )}
        {activeStep === 1 && (
          <FileUpload accept={'image/*'} setFile={setPicture}>
            <Button>Загрузить изображение</Button>
          </FileUpload>
        )}
        {activeStep === 2 && (
          <FileUpload accept={'audio/*'} setFile={setAudio}>
            <Button>Загрузить аудио</Button>
          </FileUpload>
        )}
      </StepWrapper>
      <Grid container justifyContent={'space-between'}>
        <Button disabled={activeStep === 0} onClick={handleBack}>
          Назад
        </Button>
        <Button onClick={handleNext}>Далее</Button>
      </Grid>
    </MainLayout>
  )
}
