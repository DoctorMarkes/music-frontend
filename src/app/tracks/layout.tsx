import { Metadata } from 'next'
import { MainLayoutProps } from '@/layouts/MainLayout'
import React from 'react'

export async function generateMetadata({
  title,
  keywords,
  description = '',
}: MainLayoutProps): Promise<Metadata> {
  return {
    title: title ?? 'Списко треков - музыкальная платформа',
    keywords: keywords ?? 'Музыка, треки, артисты',
    description:
      `Музыкальная площадка. Здесь каждый может оставить свой трек и стать знаменитым.` +
      description,
  }
}

export default function Layout({ children }: { children: React.ReactNode }) {
  return children
}
