'use client'
import React from 'react'
import MainLayout from '@/layouts/MainLayout'
import { Button, Grid, TextField } from '@mui/material'
import { useRouter } from 'next/navigation'
import Image from 'next/image'
import { useAddComment, useFetchTrack } from '@/services/track'
import { getFilePath } from '@/lib/utils'
import { useInput } from '@/lib/hooks'

export default function Page({ params }: { params: { id: string } }) {
  const router = useRouter()
  const { data: track, isLoading, isError, error } = useFetchTrack(params.id)
  const [addComment] = useAddComment()

  const username = useInput('')
  const text = useInput('')
  const handleAddComment = async () => {
    await addComment({
      username: username.value,
      text: text.value,
      trackId: params.id,
    })
  }

  if (isLoading) return null
  if (isError) return error

  const handleClick = () => router.push('/tracks')
  return (
    <MainLayout>
      <Button
        variant={'outlined'}
        style={{ fontSize: 32 }}
        onClick={handleClick}
      >
        К списку
      </Button>
      <Grid container style={{ margin: '20px 0' }}>
        <Image
          width={200}
          height={200}
          src={getFilePath(track?.picture)}
          alt={'picture'}
        />
        <div style={{ marginLeft: 30 }}>
          <h1>Название трека - {track?.name}</h1>
          <h2>Исполнитель - {track?.artist}</h2>
          <h2>Прослушиваний - {track?.listens}</h2>
        </div>
      </Grid>
      <h2>Слова в треке</h2>
      <p>{track?.text}</p>
      <h2>Комментарии</h2>
      <Grid container>
        <TextField
          {...username}
          style={{ marginTop: 10 }}
          label={'Ваше имя'}
          fullWidth
        />
        <TextField
          {...text}
          label={'Комментарий'}
          fullWidth
          style={{ marginTop: 10 }}
          multiline
          rows={4}
        />
        <Button style={{ marginTop: 10 }} onClick={handleAddComment}>
          Отправить
        </Button>
      </Grid>
      <div>
        {track?.comments?.map((comment) => (
          <div key={comment._id} style={{ marginTop: 10 }}>
            <div>Автор - {comment.username}</div>
            <div>Комментарий - {comment.text}</div>
          </div>
        ))}
      </div>
    </MainLayout>
  )
}
