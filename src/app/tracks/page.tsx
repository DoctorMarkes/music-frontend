'use client'
import React, { useCallback } from 'react'
import MainLayout from '@/layouts/MainLayout'
import { Box, Button, Card, Grid } from '@mui/material'
import { useRouter } from 'next/navigation'
import TrackList from '@/components/TrackList'
import { useFetchTracks } from '@/services/track'

export default function Page() {
  const router = useRouter()
  const { data: tracks = [], isLoading, isError, error = '' } = useFetchTracks()

  const handleUpload = useCallback(() => {
    router.push('/tracks/create')
  }, [router])

  if (isLoading) return null
  if (isError)
    return (
      <MainLayout>
        <h1>{error as string}</h1>
      </MainLayout>
    )

  return (
    <MainLayout title={'Списко треков - музыкальная платформа'}>
      <Grid container justifyContent={'center'}>
        <Card style={{ width: 900 }}>
          <Box p={3}>
            <Grid container justifyContent={'space-between'}>
              <h1>Список треков</h1>
              <Button onClick={handleUpload}>Загрузить</Button>
            </Grid>
          </Box>
          <TrackList tracks={tracks} />
        </Card>
      </Grid>
    </MainLayout>
  )
}
