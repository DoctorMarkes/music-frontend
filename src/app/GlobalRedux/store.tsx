'use client'
import { combineReducers, configureStore } from '@reduxjs/toolkit'
import playerSlice from './playerSlice'
import { tracksApi } from '@/services/track'

const rootReducer = combineReducers({
  player: playerSlice,
  [tracksApi.reducerPath]: tracksApi.reducer,
})

export const store = configureStore({
  reducer: rootReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(tracksApi.middleware),
})

export type AppStore = typeof store
export type RootState = ReturnType<AppStore['getState']>
export type AppDispatch = AppStore['dispatch']
