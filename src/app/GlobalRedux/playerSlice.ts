import { createSlice } from '@reduxjs/toolkit'

import { IPlayerState } from '@/types/player'

const initialState: IPlayerState = {
  active: null,
  currentTime: 3,
  duration: 0,
  pause: true,
  volume: 50,
}

const playerSlice = createSlice({
  name: 'player',
  initialState,
  reducers: {
    pauseTrack: (state) => ({ ...state, pause: true }),
    playTrack: (state) => ({ ...state, pause: false }),
    setActive: (state, action) => ({
      ...state,
      active: action.payload,
      duration: 0,
      currentTime: 0,
    }),
    setCurrentTime: (state, action) => ({
      ...state,
      currentTime: action.payload,
    }),
    setDuration: (state, action) => ({ ...state, duration: action.payload }),
    setVolume: (state, action) => ({ ...state, volume: action.payload }),
  },
})

export const {
  pauseTrack,
  playTrack,
  setDuration,
  setVolume,
  setActive,
  setCurrentTime,
} = playerSlice.actions
export default playerSlice.reducer
