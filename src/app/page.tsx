'use client'
import React from 'react'
import MainLayout from '@/layouts/MainLayout'

export default function Page() {
  return (
    <>
      <MainLayout>
        <div className="center">
          <h1>Добро пожаловать</h1>
          <h3>Здесь собраны лучшие треки!</h3>
        </div>
      </MainLayout>

      <style jsx>
        {`
          .center {
            margin-top: 150px;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
          }
        `}
      </style>
    </>
  )
}
