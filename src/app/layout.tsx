import React from 'react'
import { Metadata } from 'next'
import { MainLayoutProps } from '@/layouts/MainLayout'
import styles from '../styles/Global.module.scss'
import StoreProvider from '@/app/GlobalRedux/StoreProvider'

export async function generateMetadata({
  title,
  keywords,
  description = '',
}: MainLayoutProps): Promise<Metadata> {
  return {
    title: title ?? 'Музыкальная площадка',
    keywords: keywords ?? 'Музыка, треки, артисты',
    description:
      `Музыкальная площадка. Здесь каждый может оставить свой трек и стать знаменитым.` +
      description,
  }
}

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode
}>) {
  return (
    <html lang="en" className={styles.app}>
      <body>
        <StoreProvider>{children}</StoreProvider>
      </body>
    </html>
  )
}
