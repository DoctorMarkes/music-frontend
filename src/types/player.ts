import { ITrack } from '@/types/track'

export interface IPlayerState {
  active: null | ITrack
  volume: number
  duration: number
  currentTime: number
  pause: boolean
}

export const PlayerActionTypes = {
  PLAY: 'PLAY',
  PAUSE: 'PAUSE',
  SET_ACTIVE: 'SET_ACTIVE',
  SET_DURATION: 'SET_DURATION',
  SET_CURRENT_TIME: 'SET_CURRENT_TIME',
  SET_VOLUME: 'SET_VOLUME',
} as const

interface IPlayAction {
  type: typeof PlayerActionTypes.PLAY
}

interface IPauseAction {
  type: typeof PlayerActionTypes.PAUSE
}

interface ISetActiveAction {
  type: typeof PlayerActionTypes.SET_ACTIVE
  payload: ITrack
}

interface ISetDurationAction {
  type: typeof PlayerActionTypes.SET_DURATION
  payload: number
}

interface ISetCurrentTimeAction {
  type: typeof PlayerActionTypes.SET_CURRENT_TIME
  payload: number
}

interface ISetVolumeAction {
  type: typeof PlayerActionTypes.SET_VOLUME
  payload: number
}

export type IPlayerAction =
  | IPlayAction
  | IPauseAction
  | ISetActiveAction
  | ISetDurationAction
  | ISetCurrentTimeAction
  | ISetVolumeAction
