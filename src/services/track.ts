import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import { ITrack } from '@/types/track'

export const tracksApi = createApi({
  reducerPath: 'tracksApi',
  baseQuery: fetchBaseQuery({ baseUrl: 'http://localhost:5010' }),
  tagTypes: ['Tracks', 'Track'],
  endpoints: (builder) => ({
    fetchTracks: builder.query<ITrack[], void>({
      providesTags: ['Tracks'],
      query: () => '/tracks',
      transformErrorResponse: () => 'ошибка сервера',
    }),
    fetchTrack: builder.query<ITrack, string>({
      query: (id) => `/tracks/${id}`,
      transformErrorResponse: () => 'ошибка сервера',
      providesTags: ['Track'],
    }),
    createTrack: builder.mutation<ITrack, Partial<ITrack>>({
      query: (body) => {
        return {
          url: `/tracks`,
          method: 'POST',
          body,
        }
      },
      invalidatesTags: ['Tracks'],
    }),
    searchTracks: builder.query<ITrack[], void>({
      query: (query) => `/tracks/search?query=${query}`,
      transformErrorResponse: () => 'ошибка сервера',
    }),
    deleteTrack: builder.mutation<ITrack, string>({
      query: (id) => {
        return {
          url: `/tracks/${id}`,
          method: 'DELETE',
        }
      },
      invalidatesTags: ['Tracks'],
    }),
    addComment: builder.mutation({
      query: (body) => {
        return {
          url: '/tracks/comment',
          method: 'POST',
          body,
        }
      },
      invalidatesTags: ['Track'],
    }),
  }),
})

export const {
  useFetchTracksQuery: useFetchTracks,
  useFetchTrackQuery: useFetchTrack,
  useCreateTrackMutation: useCreateTrack,
  useAddCommentMutation: useAddComment,
  useDeleteTrackMutation,
} = tracksApi
